<?php
require('topcart.php');
?>

<!-- Welcome Section -->
<section id="gtco-welcome" >
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-sm-5 img-bg d-flex shadow align-items-center justify-content-center justify-content-md-end img-2" style="background-image: url(img/goldenn.jpg);">
                    
                </div>
                <div class="col-sm-7 py-5 pl-md-0 pl-4">
                    <div class="heading-section pl-lg-5 ml-md-5">
                        <span class="subheading">
                            About
                        </span>
                        <h2>
                            Welcome to MTT
                        </h2>
                    </div>
                    <div class="pl-lg-5 ml-md-5">
                        <p>Mountain Tea Trade Pvt. Ltd is an enterprise that distributes organic tea leaves and coffee to Nepalese and tourists visiting Nepal. The enterprise is established in 2005 A.D and located in Jyatha, Kathmandu. This company purchases the tea leaves from the local producers of Nepal. The tea leaves are packed in various types of box and bag that are made in Nepal. They are especially focused on making the tourists take tea leaves as a token from Nepal to their friends and family and help Nepalese tea known around the globe.</p>
                        <h3 class="mt-5">Popular tea</h3>
                        <div class="row">
                            <div class="col-4">
                                <a href="#" class="thumb-menu">
                                    <img class="img-fluid img-cover" src="img/ruby.jpg" />
                                    <h6>Ruby Tea</h6>
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="#" class="thumb-menu">
                                    <img class="img-fluid img-cover" src="img/gt1.jpg" />
                                    <h6>Golden Tips</h6>
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="#" class="thumb-menu">
                                    <img class="img-fluid img-cover" src="img/pearlg.jpg"/>
                                    <h6>Pearl green Tea</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Welcome Section -->		
<?php
require('footer.inc.php');
?>