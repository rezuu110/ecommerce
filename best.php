
<?php
require('topcart.php');
?>
<!-- Special tea -->
<section id="gtco-special-dishes">
    <div class="container">
        <div class="section-content">
            <div class="heading-section text-center">
                <span class="subheading">
                    Our
                </span>
                <h2>
                    BEST SELLER
                </h2>
            </div>
            <div class="row mt-5">
                <div class="col-lg-5 col-md-6 align-self-center py-5">
                    <!-- <h2 class="special-number">01.</h2> -->
                    <div class="dishes-text">
                        <h3><span>GOLDEN</span><br> TIPS</h3>
                        <p class="pt-3">Golden tips is high quality and most demanded tea. It is needle-shaped and golden in color. Golden tips tea in particular has a unique flavor and taste than ordinal teas. It has good aroma and pleasant taste. The shiny golden color of this tea will give a good appearance while consuming. It consists only of young unopened tea buds.</p>
                        
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-2 col-md-6 align-self-center mt-4 mt-md-0">
                    <img src="img/gt1.jpg" alt="" class="img-fluid shadow w-100">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-5 col-md-6 align-self-center order-2 order-md-1 mt-4 mt-md-0">
                    <img src="img/ruby.jpg" alt="" class="img-fluid shadow w-100">
                </div>
                <div class="col-lg-5 offset-lg-2 col-md-6 align-self-center order-1 order-md-2 py-5">
                    <!-- <h2 class="special-number">02.</h2> -->
                    <div class="dishes-text">
                        <h3><span>RUBY</span><br> TEA</h3>
                        <p class="pt-3">Ruby tea is premium quality black tea. This tea is hand-rolled, followed by brief and gentle mechanical rolling to give its fine twist. Ruby tea is a summer-picked oolong with a long oxidation process, giving it a strong and long-lasting flavor profile. </p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Special tea Section -->		