<?php
// session_start();
error_reporting(0);
$con = mysqli_connect("localhost", "root", "", "spdatabase");

require('topcart.php');
if (isset($_SESSION['cart'])) {
    $arra = join(",", $_SESSION['cart']);
    $q = mysqli_query($con, "SELECT * FROM product WHERE id IN ($arra)");
}

if (isset($_POST['cart-submit'])) {
    $pname = $_POST['pname'];
    $qty = $_POST['qty'];
    $price = $_POST['price'];
    $total_price = $_POST['total_price'];
    $sql = "insert into cart (`pname`,`qty`,`price`,`total_price`)values('" . $product['pname'] . "','" . $product['qty'] . "','" . $product['price'] . "',$total_price)";
    $res = mysqli_query($con, $sql);
}

// if (isset($_POST['cart-submit'])) {
//     print_r($_POST['pname']);
//     $c = 0;
//     for ($i = 0; $i < count($_POST['order_qty']); $i++) {
//         if ($_POST['qty'][$i] > $_POST['db_qty'][$i]) {
//             $c++;
//         }
//     }
//     if ($c > 0) {
//         echo "<script> alert('Quantity Not Enought to order your placement');</script>";
//     } else {
//         echo "<script> alert('Ready to process');</script>";
//     }
// }

if (isset($_POST['remove'])) {

    foreach (array_keys($_SESSION['cart'], $_POST['pid']) as $v) {
        unset($_SESSION['cart'][$v]);
    }
    echo "<script> location.replace('cart.php'); </script>";
}

// if (isset($_POST['mod_qty'])) {

//     foreach ($_SESSION['cart'] as $v) {
//         if ($v ['pname']==$_POST['pname']
//         {
//             array_keys($_SESSION['cart'] $_POST['pname'])= $_POST['mod_qty'];
//         }
//     }
//     echo "<script> location.replace('cart.php'); </script>";
// }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
</head>

<body>
    <div class="container">
        <?php
        if (isset($_SESSION['user'])) {
        ?>
            <button type="submit" class="btn btn-success mb-4 float-right" name="cart-submit" form="myform">
                Checkout
            </button>
        <?php
        }else{
            // header("location:userlogin.php");
            ?>
            <a href="userlogin.php">
                <button class="btn btn-success mb-4 float-right" name="cart-submit">Checkout</button>
            </a>
            <?php
        }
        ?>

        <div class="cart">
            <?php

            $total_price = 0;
            if (isset($_SESSION["cart"])) {
                // $total_price = 0;

            ?>
                <form action="checkoutpage.php" method="post" id="myform">

                    <table class="table">
                        <tbody>
                            <tr>
                                <td>S.N</td>
                                <td class="text-center">Product Image</td>
                                <td>Product Name</td>
                                <td>Quantity</td>
                                <td>Unit Price</td>
                                <!-- <td>Items Total</td> -->
                                <td>Action</td>
                            </tr>
                            <?php
                            // $pro = mysqli_fetch_assoc($q);
                            if (mysqli_num_rows($q) > 0) {
                                $c = 1;
                                while ($product = mysqli_fetch_assoc($q)) {
                            ?>
                                    <tr>
                                        <td><?php echo $c; ?></td>
                                        <td>
                                            <img src='media/<?php echo $product["image"]; ?>' width="70" height="70" class="rounded mx-auto d-block">
                                        </td>
                                        <td><?php echo $product["pname"]; ?><br />

                                        </td>
                                        <td>
                                            <!-- <input type="hidden" value="<?php echo $product['qty']; ?> " name="db_qty"> -->
                                            <!-- <input type="number" name="qty" id="" class="form-control" style="width: 100px;" min="1" value="1"> -->
                                            <input type="number" name="order_qty[]" min="1" value="1">
                                        </td>
                                        <td><?php echo "Rs " . $product["price"]; ?></td>
                                        <!-- <td><?php echo "Rs " . $product["price"] * $product["qty"]; ?></td> -->
                                        <td>
                                            <form method='post' action=''>
                                                <input type='hidden' name='pname' value="<?php echo $product["pname"]; ?>" />
                                                <input type='hidden' name='pid' value="<?php echo $product["id"]; ?>" />

                                                <button type='submit' class='remove btn btn-danger' name="remove"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                            <?php
                                    $total_price += ($product["price"] * $product["qty"]);
                                    $_SESSION['total'] = $total_price;
                                    $c++;
                                }
                            }
                            ?>
                            <tr>
                                <td colspan="6" align="right">
                                    <strong>TOTAL: <?php echo "Rs " . $total_price; ?></strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>

            <?php
            } else {
                echo "<h3>Your cart is empty!</h3>";
            }
            ?>
        </div>
    </div>

    <div style="clear:both;"></div>




</body>

</html>