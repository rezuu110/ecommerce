<?php
// session_start();
require('topcart.php');
//require('manage_cart.php');
$cat_id = mysqli_real_escape_string($con, $_GET['id']);
// echo $cat_id;
// die;


// $connect = mysqli_connect("localhost", "root", "", "cart");

if (isset($_POST["add_to_cart"])) {
    if (isset($_SESSION["shopping_cart"])) {
        $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
        if (!in_array($_GET["id"], $item_array_id)) {
            $count = count($_SESSION["shopping_cart"]);
            $item_array = array(
                'item_id'            =>    $_GET["id"],
                'item_name'            =>    $_POST["hidden_name"],
                'item_price'        =>    $_POST["hidden_price"],
                'item_quantity'        =>    $_POST["quantity"]
            );
            $_SESSION["shopping_cart"][$count] = $item_array;
        } else {
            echo '<script>alert("Item Already Added")</script>';
        }
    } else {
        $item_array = array(
            'item_id'            =>    $_GET["id"],
            'item_name'            =>    $_POST["hidden_name"],
            'item_price'        =>    $_POST["hidden_price"],
            'item_quantity'        =>    $_POST["quantity"]
        );
        $_SESSION["shopping_cart"][0] = $item_array;
    }
}

if (isset($_GET["action"])) {
    if ($_GET["action"] == "delete") {
        foreach ($_SESSION["shopping_cart"] as $keys => $values) {
            if ($values["item_id"] == $_GET["id"]) {
                unset($_SESSION["shopping_cart"][$keys]);
                echo '<script>alert("Item Removed")</script>';
                echo '<script>window.location="index.php"</script>';
            }
        }
    }
}

?>

<div class="container">

    <div class="row mt-5">
        <?php
        $get_product = get_product($con); //,$cat_id
        foreach ($get_product as $list) {
        ?>
            <div class="col-md-3 my-2">
                <div class="card">
                    <img class="card-img-top" src="<?php echo ('media/' . $list['image']) ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $list['pname']; ?></h5>
                        <p class="card-text">
                            Rs. <?php echo $list['price'] ?>
                        </p>
                        <p class="card-text">
                            <span>Availability: </span> In stock
                        </p>
                        <p> <span> Quantity :</span>
                            <select id="qty">
                                <option>1 </option>
                                <option>2 </option>
                                <option>3</option>
                                <option>4 </option>
                                <option>5 </option>
                                <option>6 </option>
                            </select>
                        </p>
                        <form action="catcat.php?action=add&id=<?php echo $row["id"]; ?>" method="post">
                            <input type="hidden" value="<?php echo $list['pname'];?>">
                            <input type="hidden" value="<?php echo $list['price'];?>">
                            <input type="submit" value="Add to Cart" class="btn btn-info" name="add_to_cart">
                        </form>
                        <!-- <a href="#" class="btn btn-primary" href="javascript:void(0)" onclick="manage_cart('<?php echo $get_product['0']['id'] ?>','add')">Add to Cart</a> -->

                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script src="js/app.min.js "></script>