<?php
$con=mysqli_connect("localhost","root","","spdatabase");
$msg='';
if(isset($_POST['submit']))
{
    echo $feed=get_safe_value($con,$_POST['message']);
    $sql="insert into feedback where message='$feed'";

}
?>

<footer class="mastfoot pb-5 bg-white section-padding pb-0">
    <div class="inner container">
         <div class="row">
         	<div class="col-lg-4">
         		<div class="footer-widget pr-lg-5 pr-0">
         			<img src="img/logo.png" class="img-fluid footer-logo mb-3" alt="">
	         		<p> Mountain Tea Trade Pvt. Ltd <br> We export and wholesale tea leaves</p>
	         			         		<nav class="nav nav-mastfoot justify-content-start">
		                <a class="nav-link" href="#">
		                	<i class="fab fa-facebook-f"></i>
		                </a>
		                <a class="nav-link" href="#">
		                	<i class="fab fa-twitter"></i>
		                </a>
		                <a class="nav-link" href="#">
		                	<i class="fab fa-instagram"></i>
		                </a>
		            </nav>
         		</div>
         		
         	</div>
         	<div class="col-lg-4">
         		<div class="footer-widget px-lg-5 px-0">
         			<h4>Contact us at</h4>
	         		<ul class="list-unstyled open-hours">
		                <li class="d-flex justify-content-between"><span>Jyatha,Kathmandu</span></li>
		                <li class="d-flex justify-content-between"><span>+977-9841298972</span></li>
		                <li class="d-flex justify-content-between"><span>mountainteatradepvtltd@gmail.com</span></li>

		              </ul>
         		</div>
         		
         	</div>

         	<!-- <div class="col-lg-4">
         		<div class="footer-widget pl-lg-5 pl-0">
         			<h4>Feedback</h4>
	         		<p>Share your message to us.</p>
	         		<form id="feedbacks" method="post">
						<div class="form-group">
							<input type="text" class="form-control" name="message" aria-describedby="emailNewsletter" placeholder="Your message...">
						</div>
						<button type="submit" class="btn btn-primary w-100">Send</button>
					</form>
         		</div>
         		 -->
         	</div>
         	<div class="col-md-12 d-flex align-items-center">
         		<p class="mx-auto text-center mb-0">Copyright <?php echo date('Y')?>. All Right Reserved. Mountain Tea Trade Pvt. Ltd.</p>
         	</div>
            
        </div>
    </div>
</footer>	