
<?php
require('topcart.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Resto">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- External CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/brands.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Josefin+Sans:300,400,700">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="css/style.min.css">

    <!-- Modernizr JS for IE8 support of HTML5 elements and media queries -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

</head>
<body data-spy="scroll" data-target="#navbar" class="static-layout">
	<div id="side-nav" class="sidenav">
	<a href="javascript:void(0)" id="side-nav-close">&times;</a>
	
	<div class="sidenav-content">
		
	</div>
</div>	<div id="side-search" class="sidenav">
	<a href="javascript:void(0)" id="side-search-close">&times;</a>
	<div class="sidenav-content">
		<form action="">

			<div class="input-group md-form form-sm form-2 pl-0">
			  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Search" aria-label="Search">
			  <div class="input-group-append">
			    <button class="input-group-text red lighten-3" id="basic-text1">
			    	<i class="fas fa-search text-grey" aria-hidden="true"></i>
			    </button>
			  </div>
			</div>

		</form>
	</div>
	
 	
</div>			
<div class="hero">
  <div class="container">
	<div class="row d-flex align-items-center">
		<div class="col-lg-6 hero-left">
		    <h1 class="display-4 mb-5">HAPPINESS is a  <br>cup of TEA!</h1>
		  <!---  <div class="mb-2"
		    	<a class="btn btn-primary btn-shadow btn-lg" href="#" role="button">Explore Menu</a>
			    <a class="btn btn-icon btn-lg" href="https://player.vimeo.com/video/33110953" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true">
			    	<span class="lnr lnr-film-play"></span>
			    	Play Video
			    </a>
		    </div>
            ---> 
		    <ul class="hero-info list-unstyled d-flex text-center mb-0">
		    	<!-- <li class="border-right">
		    		<span class="lnr lnr-rocket"></span>
		    		<h5>
		    			Fast Delivery
		    		</h5>
		    	</li> -->
		    	<li class="border-right">
		    		<span class="fas fa-leaf"></span>
		    		<h5>
		    			Organic leaves
		    		</h5>
		    	</li>
		    	<li class="border-right">
		    		<span class="fas fa-mug-hot"></span>
		    		<h5>
		    			Nepalese Product
		    		</h5>
		    	</li>
		    </ul>

	    </div>
	    <div class="col-lg-6 hero-right">
	    	<div class="owl-carousel owl-theme hero-carousel">
			    <div class="item">
			    	<img class="img-fluid" src="img/bpt.jpg" alt="">
			    </div>
			    <!-- <div class="item">
			    	<img class="img-fluid" src="img/teaa.jfif" alt="">
			    </div> -->
			    <div class="item">
			    	<img class="img-fluid" src="img2/packedlemon.jpg" alt="">
			    </div>
			</div>
	    </div>
	</div>
  </div>
</div>		

<!-- Welcome Section -->
<section id="gtco-welcome" class="bg-grey section-padding">
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-sm-5 img-bg d-flex shadow align-items-center justify-content-center justify-content-md-end img-2" style="background-image: url(img/goldenn.jpg);">
                    
                </div>
                <div class="col-sm-7 py-5 pl-md-0 pl-4">
                    <div class="heading-section pl-lg-5 ml-md-5">
                        <span class="subheading">
                            About
                        </span>
                        <h2>
                            Welcome to MTT
                        </h2>
                    </div>
                    <div class="pl-lg-5 ml-md-5">
                        <p>Mountain Tea Trade Pvt. Ltd is an enterprise that distributes organic tea leaves and coffee to Nepalese and tourists visiting Nepal. The enterprise is established in 2005 A.D and located in Jyatha, Kathmandu. This company purchases the tea leaves from the local producers of Nepal. The tea leaves are packed in various types of box and bag that are made in Nepal. They are especially focused on making the tourists take tea leaves as a token from Nepal to their friends and family and help Nepalese tea known around the globe.</p>
                        <h3 class="mt-5">Popular tea</h3>
                        <div class="row">
                            <div class="col-4">
                                <a href="#" class="thumb-menu">
                                    <img class="img-fluid img-cover" src="img/ruby.jpg" />
                                    <h6>Ruby Tea</h6>
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="#" class="thumb-menu">
                                    <img class="img-fluid img-cover" src="img/gt1.jpg" />
                                    <h6>Golden Tips</h6>
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="#" class="thumb-menu">
                                    <img class="img-fluid img-cover" src="img/pearlg.jpg"/>
                                    <h6>Pearl green Tea</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Welcome Section -->		

<!-- Special tea -->
<section id="gtco-special-dishes" class="bg-grey section-padding">
    <div class="container">
        <div class="section-content">
            <div class="heading-section text-center">
                <span class="subheading">
                    Our
                </span>
                <h2>
                    BEST SELLER
                </h2>
            </div>
            <div class="row mt-5">
                <div class="col-lg-5 col-md-6 align-self-center py-5">
                    <!-- <h2 class="special-number">01.</h2> -->
                    <div class="dishes-text">
                        <h3><span>GOLDEN</span><br> TIPS</h3>
                        <p class="pt-3">Golden tips is high quality and most demanded tea. It is needle-shaped and golden in color. Golden tips tea in particular has a unique flavor and taste than ordinal teas. It has good aroma and pleasant taste. The shiny golden color of this tea will give a good appearance while consuming. It consists only of young unopened tea buds.</p>
                        
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-2 col-md-6 align-self-center mt-4 mt-md-0">
                    <img src="img/gt1.jpg" alt="" class="img-fluid shadow w-100">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-5 col-md-6 align-self-center order-2 order-md-1 mt-4 mt-md-0">
                    <img src="img/ruby.jpg" alt="" class="img-fluid shadow w-100">
                </div>
                <div class="col-lg-5 offset-lg-2 col-md-6 align-self-center order-1 order-md-2 py-5">
                    <!-- <h2 class="special-number">02.</h2> -->
                    <div class="dishes-text">
                        <h3><span>RUBY</span><br> TEA</h3>
                        <p class="pt-3">Ruby tea is premium quality black tea. This tea is hand-rolled, followed by brief and gentle mechanical rolling to give its fine twist. Ruby tea is a summer-picked oolong with a long oxidation process, giving it a strong and long-lasting flavor profile. </p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Special tea Section -->			
	


<?php
require('footer.inc.php');
?>

  


	<!-- External JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="vendor/bootstrap/popper.min.js"></script>
	<script src="vendor/bootstrap/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js "></script>
	<script src="vendor/owlcarousel/owl.carousel.min.js"></script>
	<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js"></script>
	<script src="vendor/stellar/jquery.stellar.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

	<!-- Main JS -->
	<script src="js/app.min.js "></script>
</body>
</html>
