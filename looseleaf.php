<?php
require('top.inc.php');

?>
<html>
<div class="product-view">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="product-view-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="product-search">
                                                <input type="email" value="Search">
                                                <button><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="product-short">
                                                <div class="dropdown">
                                                    <div class="dropdown-toggle" data-toggle="dropdown">Product short by</div>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a href="#" class="dropdown-item">Newest</a>
                                                        <a href="#" class="dropdown-item">Popular</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="product-price-range">
                                                <div class="dropdown">
                                                    <div class="dropdown-toggle" data-toggle="dropdown">Product price range</div>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a href="#" class="dropdown-item">$0 to $10</a>
                                                        <a href="#" class="dropdown-item">$10 to $50</a>
                                                        <a href="#" class="dropdown-item">$50 to $100</a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!-- Loose leaf Section -->
<section id="gtco-team" class="bg-white section-padding">
    <div class="container">
        <div class="section-content">
            <div class="heading-section text-center">
                <span class="subheading">
                    Loose leaf tea
                </span>
                <h2>
                    Tea leaves
                </h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/gt1.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Golden Tips</h4>
                            <p class="mb-1">No.1</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/gt2.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Golden Tips</h4>
                            <p class="mb-1">No.2</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/gt3.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Golden Tips</h4>
                            <p class="mb-1">No.3</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Green -->

    <div class="row">
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/pearlg.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Pearl green tea</h4>
                            <p class="mb-1">No.1</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/organicgreen.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Organic green tea</h4>
                            <p class="mb-1">No.2</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/green.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Green tea</h4>
                            <p class="mb-1">No.3</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                        </div>
                    </div>
                </div>
            </div>
<!-- Green end -->

<!-- Row3 -->

            <div class="row">
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/white.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">White tea</h4>
                            <p class="mb-1">No.1</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/ruby.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Ruby tea</h4>
                            <p class="mb-1">No.2</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/lemon.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Lemon tea</h4>
                            <p class="mb-1">No.3</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                        </div>
                    </div>
                </div>
            </div>
<!-- Row3 end -->
<!-- Row4 -->

  
            <div class="row">
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/cardamon.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Cardamon tea</h4>
                            <p class="mb-1">No.1</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/cinamon.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Cinnamon tea</h4>
                            <p class="mb-1">No.2</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-card mb-5">
                        <img class="img-fluid" src="img/ginger.jpg" alt="">
                        <div class="team-desc">
                            <h4 class="mb-0">Ginger tea</h4>
                            <p class="mb-1">No.3</p>
                            <a class="btn" href=""><i class="fa fa-shopping-cart"></i> Add to cart</a>

                        </div>
                    </div>
                </div>
            </div>
<!-- Row4 end -->

</section>
<!-- End of Team Section -->


</html>
