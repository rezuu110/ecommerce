
<?php
require('connection.inc.php');
require('functions.inc.php');
$msg='';

//$check_user=mysqli_num_rows(mysqli_query($con,"select * from register where email='$email'"));
//if($check_user>0)
// {
//     echo"email_present";
// // }
// 
if(isset($_POST['submit']))
{
    
$fname=$_POST['fname'];
$add=$_POST['add'];
$user=$_POST['user'];
$pass=$_POST['pass'];
$cpass=$_POST['cpass'];
$phone=$_POST['phone'];
$email=$_POST['email'];

    if($pass == $cpass)
    {
    $sql=("INSERT INTO register (`fname`,`add`,`user`,`pass`,`cpass`,`phone`,`email`) VALUES ('$fname','$add','$user','$pass','$cpass','$phone','$email')");
    $res=mysqli_query($con,$sql);

    header("location:userlogin.php");
    }
else{
    $msg="Password and confirmed password doesnot match";   

  // echo("Passwords does not match");
    //header("location:member2.php");
}
}


?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register</title>
    <meta name="description" content="Resto">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- External CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/brands.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Josefin+Sans:300,400,700">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="css/style.min.css">

    <!-- Modernizr JS for IE8 support of HTML5 elements and media queries -->
    

</head>

<!-- Signup Section -->
<section id="gtco-signup"  style="background: url(img/bg12.jpg); ;">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="section-content bg-white p-3 shadow" style="position: relative; top:50%; left:50%; transform: translate(26%, -30%);">
                    <div class="heading-section text-center">
                        <span class="subheading">
                            Register
                        </span>
                        <h2>
                            Be a member now
                        </h2>
                    </div>
                    <form method="post" name="contact-us" >
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" placeholder="Full name" name="fname" id="fname" required>
                            <span class="field_error" id="fname_error"></span>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" placeholder="Address" name="add" id="add" required>
                            <span class="field_error" id="add_error"></span>
                            </div>

                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" placeholder="Username" name="user" id="user" required>
                            <span class="field_error" id="user_error"></span>
                            </div>

                            <div class="col-md-12 form-group">
                                <input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required>
                            </div>
                            <span class="field_error" id="mail_error"></span>


                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" placeholder="Password"  name="pass" id="pass" required>
                            </div>
                            <span class="field_error" id="pass_error"></span>

                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" placeholder="Confirm-Password" name="cpass" id="cpass"required> 
                            </div>
                            <span class="field_error" id="cpass_error"></span>

                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" placeholder="Phone number" name="phone" id="phone" required> 
                            </div>
                            <span class="field_error" id="phone_error"></span>


                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary btn-shadow btn-lg" type="submit" onclick="user_register()" name="submit">Register</button>
                            </div>

                        </div>
                    </form>
                    <div class="field_error"><?php  echo $msg ?></div>

                </div>
                <!-- <script type="text/javascript">
                    function valid()
                    {
                        var user=document.getElementById("user").value;
                        var mail=document.getElementById("mail").value;
                        var pw=document.getElementById("pw").value;
                        var re=document.getElementById("re").value;
        
                        if(user=="" || mail=="" || pw=="" || re=="")
                        {
                            document.getElementById("demo").innerHTML="*Please fill all this out.";
                            return false;
                        }
                        else if(user.length<8)
                        {
                            document.getElementById("demo").innerHTML="*Username should be atleast 8 characters.";
                            return false;
                            
                        }
                        else if(pw.length<10)
                        {
                            document.getElementById("demo2").innerHTML="*Password should be atleast 10 characters.";
                            return false;
                        }
                        else if(pw!=re)
                        {
                            document.getElementById("demo3").innerHTML="*Password doesn't match.";
                            return false;
                        }
                        else{
                            return true;
                        }
                    }
                </script> -->
            </div>
        </div>
        
    </div>
</section>
<!-- End of signup Section -->		

</div>
	
</div>
	<!-- External JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="vendor/bootstrap/popper.min.js"></script>
	<script src="vendor/bootstrap/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js "></script>
	<script src="vendor/owlcarousel/owl.carousel.min.js"></script>
	<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js"></script>
	<script src="vendor/stellar/jquery.stellar.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

	<!-- Main JS -->
	<!-- <script src="js/app.min.js "></script> -->
</body>
</html>
