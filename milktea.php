<?php
//session_start();
require('topcart.php');
$con=mysqli_connect("localhost","root","","spdatabase");

if (isset($_POST['button-cart'])) {
    if (isset($_SESSION['cart'])) {
        if (in_array($_POST['product_id'], $_SESSION['cart'])) {
            echo "<script> alert('Already in the cart');</script>";
            echo  "<script> location.replace('milktea.php');</script>";
            
        } else {  
            array_push($_SESSION['cart'], $_POST['product_id']);
            echo "<script> alert('Added to cart');</script>";
            echo  "<script> location.replace('milktea.php');</script>";
            
        }
    } else {
        // print_r($_SESSION['cart']);
        $_SESSION['cart'][0] = $_POST['product_id'];
        echo "<script> alert('Added to cart');</script>";
        echo  "<script> location.replace('milktea.php');</script>";
        
    }
}
?>

<!-- Milk tea Section -->
<section id="gtco-team" class="bg-white">
<div class="container">
    <h3 class="text-center">Milk Tea</h3>
    <div class="row">
        <?php
        $get_product = mysqli_query($con, "SELECT * FROM product where categories_id=10 and status=1");
        while ($list = mysqli_fetch_assoc($get_product)) {
        ?>
            <div class="col-md-3 my-2">
                <div class="card">
                    <img class="card-img-top" src="<?php echo ('media/' . $list['image']) ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title<?php echo $list['id']; ?>"><?php echo $list['pname']; ?></h5>
                        <p class="card-text">
                            Rs. <?php echo $list['price']; ?>
                        </p>
                        <p class="card-text">
                        <span>Weight:<?php echo $list['weight']; ?>  grams </span>
                        </p>
                        <!-- <p> <span> Quantity :</span>
                            <select id="qty">
                                <option>1 </option>
                                <option>2 </option>
                                <option>3</option>
                                <option>4 </option>
                                <option>5 </option>
                                <option>6 </option>
                                <option>7 </option>
                                <option>8 </option>
                                <option>9 </option>
                                <option>10 </option>
                            </select>
                        </p> -->
                        <form action="" method="post">
                            <input type="hidden" value="<?php echo $list['id']; ?>" name="product_id">
                            <button type="submit" name="button-cart" class="btn btn-success buy" id="cart-button<?php echo $list['id']; ?>">Add To Cart</button>
                        </form>

                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>

<div style="clear:both;"></div>

<div class="message_box" style="margin:10px 0px;">
    <?//php echo $status; ?>
</div>
</section>
<!-- End of Milk Tea Section -->
</html>