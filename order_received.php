<?php
require('topadmin.php');

$sql = "SELECT * FROM order_tbl JOIN register ON order_tbl.customer_id = register.id";
$res = mysqli_query($con, $sql);


?>


<!-- <div class="col-lg-4 sidebar">
                        <div class="sidebar-widget category">
                            <h2 class="title">Menu</h2>
                            <nav class="navbar bg-light">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="categories.php">Category</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="product.php">Products</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="wholesale2.php">Wholesale contacts</a>
                                    </li>
                                  
                                </ul>
                            </nav>
                        </div>
                    </div>
 -->

<!-- Cart Start -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="cart-page-inner">
                <!-- <div class="table-responsive"> -->
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <h4 class="box-title"> Orders </h4>

                        <tr>
                            <th>OrderID</th>
                            <th>Name</th>
                            <!-- <th>Address</th> -->
                            <th>Email</th>
                            <th>Phone number</th>
                            <th>Date</th>
                            <th>Action</th>

                            <!-- <th>Delete</th> -->


                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($row = mysqli_fetch_assoc($res)) {
                        ?>
                            <tr>
                                <td> <?php echo $row['order_id'] ?></td>
                                <td> <?php echo $row['user'] ?></td>
                                <!-- <td> <?php echo $row['address'] ?></td> -->
                                <td> <?php echo $row['email'] ?></td>
                                <td> <?php echo $row['phone'] ?></td>
                                <td> <?php echo $row['order_date'] ?></td>

                                <td>
                                    <a href="view_order.php?id=<?php echo $row['order_id']; ?>">
                                        <button class="btn btn-success">View</button>
                                    </a>
                                </td>
                                <!-- <td> <?php echo $row['paymode'] ?></td> -->

                            </tr>
                        <?php   } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>