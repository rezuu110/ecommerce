<?php
require('topadmin.php');

if (isset($_GET['type'])&& $_GET['type']!='')
{
    $type=$_GET['type'];
    if($type=='status')
    {
        $operation=$_GET['operation'];
        $id=$_GET['id'];
        if($operation=='active')
        {
            $status='1';
        }
        else
        {
            $status='0';

        }
        $update_status_sql="update product set status='$status' where id='$id'";  
        mysqli_query($con,$update_status_sql);
    }

    if($type=='delete')
    {
        $id=$_GET['id'];
        $delete_sql="delete from product where id='$id'";  
        mysqli_query($con,$delete_sql);
    }

   
}
$sql="select product.*,categories.categories from product, categories where product.categories_id=categories.id order by product.id desc";
$res=mysqli_query($con,$sql);


?>



<!-- Cart Start -->
<div class="cart-page">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-15">
                        <div class="cart-page-inner">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <h2 class="box-title"> Products </h2>
                                        <h4 class="box-link">  <a href="add_product.php">
                        <button class="btn btn-success my-4">Add product</button>
                    </a> </h4>
                    <!-- <a href="add_product.php"> Add product </h4> -->
                                      
                                        <tr>
                                        <!-- <th>#</th> -->

                                            <th>ID</th>
                                            <th>Category</th>
                                            <th>Name</th>
                                            <th>Weight</th>                      
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Image</th>
                                            <th>Short description</th>
                                            <th>Description</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        //   $i=1;
                                          while($row=mysqli_fetch_assoc($res)) 
                                          {?>
                                          <tr>
                                            <!-- <td class="thead-dark"> </td> -->
                                            <td> <?php echo $row['id']?></td>
                                            <td> <?php echo $row['categories']?></td>
                                            <td> <?php echo $row['pname']?></td>
                                            <td> <?php echo $row['weight']?> grams</td>
                                            <td> Rs.<?php echo $row['price']?></td>
                                            <td> <?php echo $row['qty']?></td>
                                            <td> <img src="media/<?php echo $row['image']?>"width=100px height=100px></td>
                                            <td> <?php echo $row['shortdes']?></td>
                                            <td> <?php echo $row['des']?></td>


                                            <td> 
                                                <?php 
                                                if ($row['status']==1)
                                                {
                                                    echo"<span class='activecat'> <a href='?type=status&operation=deactive&id=".$row['id']."'> Active</a></span>&nbsp";
                                                }
                                                else 
                                                {
                                                    echo"<span class='deactivecat'><a href='?type=status&operation=active&id=".$row['id']."'> Deactive</a></span>&nbsp";
                                                }
                                                
                                                echo"<span class='editcat'><a href='add_product.php?id=".$row['id']."'> Edit </a></span>&nbsp";
                                                
                                                echo"<span class='deletecat'> <a href='?type=delete&id=".$row['id']."'> Delete</a></span>&nbsp";


                                                ?>
                                                </td>
                                          </tr>
                                          <?php   } ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


