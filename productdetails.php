<?php
require('topcart.php');
?>
<!-- Special tea -->
<section id="gtco-special-dishes">
    <div class="container">
        <div class="section-content">
            <div class="row mt-5">
                <div class="col-lg-5 col-md-6 align-self-center order-2 order-md-1 mt-4 mt-md-0">
                    <img src="img/ruby.jpg" alt="" class="img-fluid shadow w-100">
                </div>
                <div class="col-lg-5 offset-lg-2 col-md-6 align-self-center order-1 order-md-2 py-5">
                    <div class="dishes-text">
                        <h3><span>RUBY TEA</h3>
                        <p class="pt-3">Ruby tea is premium quality black tea.</p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Special tea Section -->		

       