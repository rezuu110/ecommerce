<?php
require('connection.inc.php');
require('functions.inc.php');
$msg='';

if(isset($_POST['submit']))
{
    $cname=$_POST['cname'];
    $cpass=$_POST['cpass'];

    $res=mysqli_query($con,"select * from register where user='$cname'and pass='$cpass'");
    $f = mysqli_fetch_assoc($res);
    $check_user=mysqli_num_rows($res);

    if($check_user>0)
	{
		$_SESSION['user']=$cname;
		$_SESSION['pass']=$cpass;
		$_SESSION['id']=$f['id'];
		echo ($_SESSION['user']);
		echo $_SESSION['id'];
		// die();
		// if(isset($_POST['rem_user']))
		// {
		// 	setcookie("user","$username",time()+(60*60*25));
		// 	setcookie("pass","$password",time()+(60*60*25));
		// }
		header('location:home.php');
		die();
	}	
    else
    {
        $msg="Please enter correct details";   
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="description" content="Resto">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- External CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/brands.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Josefin+Sans:300,400,700">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="css/style.min.css">

    <!-- Modernizr JS for IE8 support of HTML5 elements and media queries -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

</head>

<!-- Customer Section -->
<section id="gtco-signup" class="bg-fixed bg-white section-padding overlay" style="background: url(img/bg.jpg); ">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-content bg-white p-5 shadow">
                    <div class="heading-section text-center">
                    <span class="subheading">
                            User
                        </span>
                        <h2>
                         Login
                        </h2>
                    </div>
                    <form method="post" name="contact-us">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" placeholder="Enter Username" name="cname" id="cname"required>
                            </div>
                          
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" placeholder="Enter Password"  name="cpass" id="cpass" required >
                            </div>
                           
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary btn-shadow btn-lg" type="submit" name="submit">Log in</button>
                            </div>
                            
                            <div class="col-md-12 text-center mt-4">
                                <hr>
                                <a href="member2.php">Register Here</a>
                            </div>
                        </div>
                    </form>
                    <div class="field_error"><?php  echo $msg ?></div>

                </div>  
            </div>
        </div>
    </div>
</section>
<!-- End of Customer Section -->		


</div>
	
</div>
	<!-- External JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="vendor/bootstrap/popper.min.js"></script>
	<script src="vendor/bootstrap/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js "></script>
	<script src="vendor/owlcarousel/owl.carousel.min.js"></script>
	<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js"></script>
	<script src="vendor/stellar/jquery.stellar.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

	<!-- Main JS -->
	<script src="js/app.min.js "></script>
</body>
</html>

