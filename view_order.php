<?php
require('topadmin.php');

$id = $_GET['id'];
$query = "SELECT * FROM register JOIN order_tbl ON register.id = order_tbl.customer_id JOIN order_list ON order_list.order_id = order_tbl.order_id WHERE order_list.order_id = $id";
$result = mysqli_query($con, $query);
$resu = mysqli_query($con, $query);
$r = mysqli_fetch_assoc($resu);
?>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-lg-6">
            <div class="cart-page-inner">
                <table class="table">
                    <tr>
                        <th>Order Id:</th>
                        <th><?php echo $id; ?></th>
                    </tr>
                    <tr>
                        <th>Customer Id:</th>
                        <th><?php echo $r['id'] ?></th>
                    </tr>
                    <tr>
                        <th>Reciever Name:</th>
                        <th><?php echo $r['fname'] ?></th>
                    </tr>
                    <tr>
                        <th>Address:</th>
                        <th><?php echo $r['add'] ?></th>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <th><?php echo $r['email']; ?></th>
                    </tr>
                    <tr>
                        <th>Phone Number:</th>
                        <th><?php echo $r['phone']; ?></th>
                    </tr>
                </table>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    <div class="row">
        <table class="table table-bordered">
            <thead class="">
                <tr>
                    <a href="">
                        <button class="btn btn-success my-4">Out for Delivery</button>
                    </a>
                </tr>
                <tr>
                    <th>S.N</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $c = 1;
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <tr>
                        <td><?php echo $c; ?></td>
                        <td><?php echo $row['item_name']; ?></td>
                        <td><?php echo $row['price']; ?></td>
                        <td><?php echo $row['qty']; ?></td>
                    </tr>
                <?php
                    $c++;
                }
                ?>



            </tbody>
    </div>
</div>